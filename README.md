# README #

Automated lung nodule detection using deep convolutional neural networks. 

[MatConvNet](http://www.vlfeat.org/matconvnet/) (a deep learning toolbox for Matlab) is required to train a new network, a pre-trained model is provided under data/nodulecnn.mat. This is compiled for a Mac 64-bit system, you may need to recompile it accordingly.