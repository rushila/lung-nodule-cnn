function imbw = im_filter_seg(img)

se1 = strel('disk',15);
se2 = strel('disk',10);

im = imerode(img,se1);
im = imdilate(im,se2);

level = graythresh(im);
imbw = ~im2bw(im,level);
