function out = post_process(spx,score,param)
score = score./max(score(:));

S = real(spx(:));
Sc = zeros(1,length(S));
P = score(:);
N = unique(S);

theta1 = param(1);
theta2 = param(2);
for n = 1:length(N)
    
    idx = (S == N(n));
    K = sum(idx);
    probs = P(idx);
    if sum(probs>theta1)>theta2*K
        Sc(idx) = 1;
    end
    
end
out = reshape(Sc,size(spx));
