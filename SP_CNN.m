function SP_CNN(idx)

file1 = sprintf('annotated_data/Results/cnn-test-prediction-%.3d.csv',idx);
file2 = sprintf('matlab_superpixel_label/result/%.3d.pnglabel.mat',idx);
te = load('annotated_data/Test/testing_images.mat');

imbw = im_filter_seg(te.te_img(:,:,idx));
Tmp = load(file1);
T = Tmp(1,:);
L1 = Tmp(2,:);
Sc = zeros(512,512);
Lb = zeros(512,512);

Sc(11:512-11,11:512-11) = reshape(T,491,491)';
Scmap = Sc.*imbw;
Lb(11:512-11,11:512-11) = reshape(L1,491,491)';

spx =load(file2);
clean = post_process(spx.s,Scmap,[0.5 0.5]);
stats = getcm(Lb(:),clean(:));
rec = stats.confusionMat(2,:);
prec = stats.confusionMat(1,:);
%disp(stats.confusionMat)
corr(idx) = prec(2)/(rec(2)+rec(1));
fpr(idx) = prec(2)/(prec(2)+rec(2));
acc(idx) = rec(2)/(rec(1)+rec(2));
subplot(151),imagesc(te.te_img(:,:,idx));
axis off
title('Original Image','FontSize',15)

subplot(152),imagesc(double(te.te_img(:,:,idx)).*imbw);
axis off
title('Segmented Lung','FontSize',15)

subplot(153),imagesc(Scmap);
axis off
title('Nodule Probability Map','FontSize',15)
str = sprintf('Accuracy = %2.2f,\n False Positive Rate = %2.2f.',acc(idx),fpr(idx));
text(-0,550,str,'FontSize',15)

subplot(154),imagesc(clean);
axis off
title('Prediction','FontSize',15);

subplot(155),imagesc(Lb);
axis off
title('Ground Truth','FontSize',15)


disp(corr(idx)/acc(idx))

disp(stats.confusionMat)
disp(L(idx,:))
%figure,imagesc(Lb)