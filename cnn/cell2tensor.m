function [tensor] = cell2tensor(celldat)
%converts a 2D cell of the form MxN where each element contains
% a RxC array, into a 3D tensor of the form (R,C,M*N)
C = celldat;
[r,c] = size(C{1,1});
d = round(sqrt(r));
x = cell2mat(C);
array3d = permute(reshape(x,d,d,[]),[1 2 3]) ;
tensor  = array3d;
end

