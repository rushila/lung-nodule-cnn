function feats = create_test_data(id)
load annotated_data/Test/testing_images.mat
load annotated_data/Test/GT_label.mat
w = 10;
D = size(te_img,3);

d = id;

im = te_img(:,:,d);
m = mask(:,:,d);
feats = feature_per_pixel_full(im,m,w);
filename = sprintf('annotated_data/Test/test-features-%0.3d.csv',d);
disp('\n')
disp(filename)
csvwrite(filename,feats);

