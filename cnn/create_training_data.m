function create_training_data()
load annotated_data/Train/training_images.mat
load annotated_data/Train/GT_label.mat
w = 10;
tr_feats = features_per_vol_cnn(tr_img,mask,w);
csvwrite('annotated_data/Train/training.csv',tr_feats);