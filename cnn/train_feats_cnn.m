function [feats,label] = train_feats_cnn(imdb,mask,w)

N = size(imdb,3);


filename = ['data/','cnn_train_features_',num2str(w),'.mat'];
if ~exist(filename,'file')
    [feat_pos, feat_neg] = features_per_vol_cnn(im,mask,w);
    feats = cat(3,feat_pos,feat_neg);
    label = [ones(1,size(feat_pos,3)) 2*ones(1,size(feat_neg,3))];
    save(filename,'feats','label');
else 
    load(filename)
end
disp('completed extracting features, beginning training')


