function tr_feats = features_per_vol_cnn(vol,mask,w)
tr_feats = [];

D = size(vol,3);

for d = 1:D
    im = vol(:,:,d);
    imbw = im_filter_seg(im);
    im = im.*imbw;
    m = mask(:,:,d);
    feats = feature_per_pixel(im,m,w);
    tr_feats = cat(1,tr_feats,feats);
    str = sprintf(' \n completed processing for slice # %d\n',d);
    disp(str)
end