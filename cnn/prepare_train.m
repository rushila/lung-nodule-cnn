function [xtrain3d, label] = prepare_train()
x_train = load('annotated_data/Train/training.csv');
label = x_train(:,end);
x_train(:,end) = [];
x_train = bsxfun(@minus,x_train,mean(x_train,1));
s = sqrt(length(x_train(1,:)));
xtrain3d = reshape(x_train',s,s,[]) ;

