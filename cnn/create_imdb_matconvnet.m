function images = create_imdb_matconvnet(training_data,labels)
N = size(training_data,3);
randidx = randperm(N,N);
labels = labels(randidx);
tr_data = training_data(:,:,randidx);

v = floor(0.95*N);

db = struct(...
    'id',1:N,...
    'data',single(tr_data),...
    'label',labels'+1,...
    'set',[ones(1,v) 2*ones(1,N-v)]...
    );

images = db;
save('data/cnn_db.mat','images');