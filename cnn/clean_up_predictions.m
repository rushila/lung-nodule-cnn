function p = clean_up_predictions(y)
N = length(y);
z = y.*(y>0);
mz = sum(z);
p = z./(mz);