function [Tmp,preds,labels] = cnn_predict_nodule(net,test_id,varargin)
[feats,labels] = prepare_test(test_id);
%[feats,labels] = prepare_test_fly(test_id);
%[feats,labels] = prepare_test_img(varargin{1});
w = 10;
% Apply the CNN to the larger image
[d1,d2,d3] = size(feats);
preds = zeros(1,d3);
tic
parfor i = 1:d3
    res = vl_simplenn(net, single(feats(:,:,i))) ;
    y = squeeze(res(end).x);
    p = clean_up_predictions(y);
    preds(i) = p(2);
end

scores = reshape(preds,sqrt(d3),sqrt(d3));
filename = sprintf('annotated_data/Results/cnn-test-prediction-%.3d.csv',test_id);
S = [preds;labels'];
csvwrite(filename,S);

toc
load('annotated_data/Test/testing_images.mat','te_img');
im = te_img(:,:,test_id);
%load('UCSF/ucsf_slices.mat');
%im = img(:,:,test_id);

Tmp = zeros(size(im));
Tmp(w+1:size(im,1)-w-1,w+1:size(im,2)-w-1) = scores';
subplot(121),imagesc(im)
subplot(122),imagesc(Tmp);
colormap('jet');