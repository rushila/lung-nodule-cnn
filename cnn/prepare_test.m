function [xtest3d, label] = prepare_test(id)
filename = sprintf('annotated_data/Test/test-features-%.3d.csv',id);
if ~exist(filename,'file')
   x_test = create_test_data(id);
else
    x_test = load(filename);
end

label = x_test(:,end);
x_test(:,end) = [];
x_test = bsxfun(@minus,x_test,mean(x_test,1));
s = sqrt(length(x_test(1,:)));
xtest3d = reshape(x_test',s,s,[]) ;

