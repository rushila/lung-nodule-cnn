clc
clear
warning off
addpath(genpath(('cnn')))
test_id = 1;
%% --------------Create Training Dataset-------------------------------------
if ~exist('annotated_data/Train/training.csv','file')
    create_training_data();
end

%% -------------Training a CNN----------------------------------------------
[feats,labels] = prepare_train();
out = create_imdb_matconvnet(feats,labels);

%% if a trained network exists in data/nodulecnn.mat, 
%%this will load that. Else it will train a new one. 

net = custom_cnn_train(); 

%% -------------Testing-----------------------------------------------------
score_map = cnn_predict_nodule(net,test_id);
%% ------------ Cleaning up with super-pixels-------------------------------
SP_CNN(test_id)
